package broker

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/loranna/configuration/internal/configuration"
)

type Config struct {
	RabbitMQServerURL string `yaml:"rabbitmqserverurl" json:"rabbitmqserverurl" mapstructure:"rabbitmqserverurl"`
	RabbitMQUsername  string `yaml:"rabbitmqusername" json:"rabbitmqusername" mapstructure:"rabbitmqusername"`
	RabbitMQPassword  string `yaml:"rabbitmqpassword" json:"rabbitmqpassword" mapstructure:"rabbitmqpassword"`
	RabbitMQPort      uint   `yaml:"rabbitmqport" json:"rabbitmqport" mapstructure:"rabbitmqport"`
}

var varstrings = [...]configuration.ParameterString{
	{Id: "rabbitmqserverurl", EnvVariableName: "RABBITMQ_SERVER_URL", Description: "URL of the RabbitMQ server used for data exchange", DefaultValue: "localhost"},
	{Id: "rabbitmqusername", EnvVariableName: "RABBITMQ_USERNAME", Description: "User Name of the RabbitMQ server used for data exchange", DefaultValue: "guest"},
	{Id: "rabbitmqpassword", EnvVariableName: "RABBITMQ_PASSWORD", Description: "Password of the RabbitMQ server used for data exchange", DefaultValue: "guest"},
}
var varints = [...]configuration.ParameterInt{
	{Id: "rabbitmqport", EnvVariableName: "RABBITMQ_PORT", Description: "Port of the RabbitMQ server used for data exchange", DefaultValue: 5672},
}

func AddAndBindFlags(cmd *cobra.Command, v *viper.Viper) {
	for i := 0; i < len(varstrings); i++ {
		configuration.AddStringflag(cmd, &varstrings[i])
		configuration.BindConfig(cmd, v, &varstrings[i])
	}
	for i := 0; i < len(varints); i++ {
		configuration.AddIntflag(cmd, &varints[i])
		configuration.BindConfig(cmd, v, &varints[i])
	}
}

func BindEnv(v *viper.Viper) {
	for i := 0; i < len(varstrings); i++ {
		configuration.BindEnv(v, &varstrings[i])
	}
	for i := 0; i < len(varints); i++ {
		configuration.BindEnv(v, &varints[i])
	}
}

func LoadConfig(v *viper.Viper) (*Config, error) {
	c := Config{}
	err := v.UnmarshalExact(&c)
	if err != nil {
		return nil, err
	}
	return &c, nil
}

func Env(v *viper.Viper) []string {
	envs := []string{}
	for i := 0; i < len(varints); i++ {
		env := fmt.Sprintf("%s=%s", varints[i].EnvironmentVariableName(), v.GetString(varints[i].ID()))
		envs = append(envs, env)
	}
	for i := 0; i < len(varstrings); i++ {
		env := fmt.Sprintf("%s=%s", varstrings[i].EnvironmentVariableName(), v.GetString(varstrings[i].ID()))
		envs = append(envs, env)
	}
	return envs
}
