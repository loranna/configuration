module gitlab.com/loranna/configuration

go 1.14

require (
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.1
)
