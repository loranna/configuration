package gateway

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/loranna/configuration/internal/configuration"
)

type Config struct {
	RabbitMQUrl           string  `yaml:"rabbitmqserverurl" json:"rabbitmqserverurl" mapstructure:"rabbitmqserverurl"`
	RabbitMQUsername      string  `yaml:"rabbitmqusername" json:"rabbitmqusername" mapstructure:"rabbitmqusername"`
	RabbitMQPassword      string  `yaml:"rabbitmqpassword" json:"rabbitmqpassword" mapstructure:"rabbitmqpassword"`
	RabbitMQPort          uint    `yaml:"rabbitmqport" json:"rabbitmqport" mapstructure:"rabbitmqport"`
	Deviceupqueuename     string  `yaml:"deviceupqueuename" json:"deviceupqueuename" mapstructure:"deviceupqueuename"`
	Gatewaydownqueuename  string  `yaml:"gatewaydownqueuename" json:"gatewaydownqueuename" mapstructure:"gatewaydownqueuename"`
	NetworkInterfaceName  string  `yaml:"networkinterfacename" json:"networkinterfacename" mapstructure:"networkinterfacename"`
	MacAddress            string  `yaml:"macaddress" json:"macaddress" mapstructure:"macaddress"`
	Longitude             float64 `yaml:"longitude" json:"longitude" mapstructure:"longitude"`
	Latitude              float64 `yaml:"latitude" json:"latitude" mapstructure:"latitude"`
	EnvironmentServerURL  string  `yaml:"environmentserverurl" json:"environmentserverurl" mapstructure:"environmentserverurl"`
	EnvironmentServerPort uint    `yaml:"environmentserverport" json:"environmentserverport" mapstructure:"environmentserverport"`
	Duplexity             string  `yaml:"duplexity" json:"duplexity" mapstructure:"duplexity"`
	LoriotServerURL       string  `yaml:"loriotserverurl" json:"loriotserverurl" mapstructure:"loriotserverurl"`
	ExpectedRssi          float64 `yaml:"expectedrssi" json:"expectedrssi" mapstructure:"expectedrssi"`
	RssiBias              float64 `yaml:"rssibias" json:"rssibias" mapstructure:"rssibias"`
	ExpectedSnr           float64 `yaml:"expectedsnr" json:"expectedsnr" mapstructure:"expectedsnr"`
	SnrBias               float64 `yaml:"snrbias" json:"snrbias" mapstructure:"snrbias"`
	ExpectedLatency       float64 `yaml:"expectedlatency" json:"expectedlatency" mapstructure:"expectedlatency"`
	RadioModel            string  `yaml:"radiomodel" json:"radiomodel" mapstructure:"radiomodel"`
	Groups                []uint  `yaml:"groups" json:"groups" mapstructure:"groups"`
}

var varstrings = [...]configuration.ParameterString{
	{Id: "rabbitmqserverurl", EnvVariableName: "RABBITMQ_SERVER_URL", Description: "URL of the RabbitMQ server used for data exchange", DefaultValue: "localhost"},
	{Id: "rabbitmqusername", EnvVariableName: "RABBITMQ_USERNAME", Description: "User Name of the RabbitMQ server used for data exchange", DefaultValue: "guest"},
	{Id: "rabbitmqpassword", EnvVariableName: "RABBITMQ_PASSWORD", Description: "Password of the RabbitMQ server used for data exchange", DefaultValue: "guest"},
	{Id: "deviceupqueuename", EnvVariableName: "DEVICE_UP_QUEUE_NAME", Description: "Name of the queue used for the devie uplink to the environment messages", DefaultValue: "uplinktoenvironment"},
	{Id: "gatewaydownqueuename", EnvVariableName: "GATEWAY_DOWN_QUEUE_NAME", Description: "Name of the queue used for the gateway downlink to the environment messages", DefaultValue: "downlinktoenvironment"},
	{Id: "networkinterfacename", EnvVariableName: "NETWORK_INTERFACE_NAME", Description: "Name of the default interface used by the gateway"},
	{Id: "macaddress", EnvVariableName: "MAC_ADDRESS", Description: "MAC/HW address of the gateway"},
	{Id: "environmentserverurl", EnvVariableName: "ENVIRONMENT_SERVER_URL", Description: "URL of the environment used to manipulate messages in both directions"},
	{Id: "duplexity", EnvVariableName: "DUPLEXITY", Description: "duplexity of the gateway. Possible values: fullduplex,halfduplex", DefaultValue: "fullduplex"},
	{Id: "radiomodel", EnvVariableName: "RADIO_MODEL", Description: "radio model of the gateway. Possible values: simple", DefaultValue: "simple"},
	{Id: "loriotserverurl", EnvVariableName: "LORIOT_SERVER_URL", Description: "URL of the LORIOT server used for data processing"},
}

var varfloat64s = [...]configuration.ParameterFloat64{
	{Id: "longitude", EnvVariableName: "LONGITUDE", Description: "Longitude of the gateway", DefaultValue: 47.515086},
	{Id: "latitude", EnvVariableName: "LATITUDE", Description: "Latitude of the gateway", DefaultValue: 19.078076},
	{Id: "expectedrssi", EnvVariableName: "EXPECTED_RSSI", Description: "Value of the expected RSSI attached to uplinks", DefaultValue: -55},
	{Id: "rssibias", EnvVariableName: "RSSI_BIAS", Description: "Bias of the attached RSSI", DefaultValue: 2},
	{Id: "expectedsnr", EnvVariableName: "EXPECTED_SNR", Description: "Value of the expected SNR attached to uplinks", DefaultValue: 0},
	{Id: "snrbias", EnvVariableName: "SNR_BIAS", Description: "Bias of the attached SNR", DefaultValue: 1},
	{Id: "expectedlatency", EnvVariableName: "EXPECTED_LATENCY", Description: "Expected Latency of the gateway", DefaultValue: 0},
}

var varints = [...]configuration.ParameterInt{
	{Id: "rabbitmqport", EnvVariableName: "RABBITMQ_PORT", Description: "Port of the RabbitMQ server used for data exchange", DefaultValue: 5672},
	{Id: "environmentserverport", EnvVariableName: "ENVIRONMENT_SERVER_PORT", Description: "Port of the environment used to manipulate messages in both directions", DefaultValue: 35999},
}

var varuintslice = []configuration.ParameterUintSlice{
	{Id: "groups", EnvVariableName: "GROUPS", Description: "comma separated list of group IDs which the device is the member of", DefaultValue: []uint{0}},
}

func AddAndBindFlags(cmd *cobra.Command, v *viper.Viper) {
	for i := 0; i < len(varstrings); i++ {
		configuration.AddStringflag(cmd, &varstrings[i])
		configuration.BindConfig(cmd, v, &varstrings[i])
	}
	for i := 0; i < len(varints); i++ {
		configuration.AddIntflag(cmd, &varints[i])
		configuration.BindConfig(cmd, v, &varints[i])
	}
	for i := 0; i < len(varfloat64s); i++ {
		configuration.AddFloat64flag(cmd, &varfloat64s[i])
		configuration.BindConfig(cmd, v, &varfloat64s[i])
	}
	for i := 0; i < len(varuintslice); i++ {
		configuration.AddUintSliceflag(cmd, &varuintslice[i])
		configuration.BindConfig(cmd, v, &varuintslice[i])
	}
}

func BindEnv(v *viper.Viper) {
	for i := 0; i < len(varstrings); i++ {
		configuration.BindEnv(v, &varstrings[i])
	}
	for i := 0; i < len(varints); i++ {
		configuration.BindEnv(v, &varints[i])
	}
	for i := 0; i < len(varfloat64s); i++ {
		configuration.BindEnv(v, &varfloat64s[i])
	}
	for i := 0; i < len(varuintslice); i++ {
		configuration.BindEnv(v, &varuintslice[i])
	}
}

func LoadConfig(v *viper.Viper) (*Config, error) {
	c := Config{}
	//workaround for uint slice
	//https://github.com/spf13/viper/issues/926
	if value, ok := v.Get("groups").(string); ok {
		v.Set("groups", strings.Trim(value, "[]"))
	}
	//workaround ended
	err := v.UnmarshalExact(&c)
	if err != nil {
		return nil, err
	}
	return &c, nil
}

func Env(v *viper.Viper) []string {
	envs := []string{}
	for i := 0; i < len(varints); i++ {
		env := fmt.Sprintf("%s=%s", varints[i].EnvironmentVariableName(), v.GetString(varints[i].ID()))
		envs = append(envs, env)
	}
	for i := 0; i < len(varstrings); i++ {
		env := fmt.Sprintf("%s=%s", varstrings[i].EnvironmentVariableName(), v.GetString(varstrings[i].ID()))
		envs = append(envs, env)
	}
	for i := 0; i < len(varfloat64s); i++ {
		env := fmt.Sprintf("%s=%s", varfloat64s[i].EnvironmentVariableName(), v.GetString(varfloat64s[i].ID()))
		envs = append(envs, env)
	}
	for i := 0; i < len(varuintslice); i++ {
		a := v.Get(varuintslice[i].ID()).([]interface{})
		env := fmt.Sprintf("%s=%s", varuintslice[i].EnvironmentVariableName(),
			strings.ReplaceAll(strings.Trim(fmt.Sprintf("%v", a), "[]"), " ", ","))
		envs = append(envs, env)
	}
	return envs
}
