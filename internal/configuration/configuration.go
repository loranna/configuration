package configuration

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

type Parameter interface {
	ID() string
	EnvironmentVariableName() string
}

type ParameterInt struct {
	Id, EnvVariableName, Description string
	DefaultValue                     int
	Value                            int
}

func (p *ParameterInt) ID() string {
	return p.Id
}

func (p *ParameterInt) EnvironmentVariableName() string {
	return p.EnvVariableName
}

type ParameterString struct {
	Id, EnvVariableName, Description string
	DefaultValue                     string
	Value                            string
}

func (p *ParameterString) EnvironmentVariableName() string {
	return p.EnvVariableName
}

func (p *ParameterString) ID() string {
	return p.Id
}

type ParameterFloat64 struct {
	Id, EnvVariableName, Description string
	DefaultValue                     float64
	Value                            float64
}

func (p *ParameterFloat64) ID() string {
	return p.Id
}

func (p *ParameterFloat64) EnvironmentVariableName() string {
	return p.EnvVariableName
}

type ParameterBool struct {
	Id, EnvVariableName, Description string
	DefaultValue                     bool
	Value                            bool
}

func (p *ParameterBool) ID() string {
	return p.Id
}

func (p *ParameterBool) EnvironmentVariableName() string {
	return p.EnvVariableName
}

type ParameterUintSlice struct {
	Id, EnvVariableName, Description string
	DefaultValue                     []uint
	Value                            []uint
}

func (p *ParameterUintSlice) ID() string {
	return p.Id
}

func (p *ParameterUintSlice) EnvironmentVariableName() string {
	return p.EnvVariableName
}

func AddStringflag(cmd *cobra.Command, parameter *ParameterString) {
	if cmd.Flags().Lookup(parameter.ID()) == nil {
		cmd.Flags().StringVar(&parameter.Value, parameter.ID(), parameter.DefaultValue, parameter.Description)
	}
}

func AddIntflag(cmd *cobra.Command, parameter *ParameterInt) {
	if cmd.Flags().Lookup(parameter.ID()) == nil {
		cmd.Flags().IntVar(&parameter.Value, parameter.ID(), parameter.DefaultValue, parameter.Description)
	}
}

func AddFloat64flag(cmd *cobra.Command, parameter *ParameterFloat64) {
	if cmd.Flags().Lookup(parameter.ID()) == nil {
		cmd.Flags().Float64Var(&parameter.Value, parameter.ID(), parameter.DefaultValue, parameter.Description)
	}
}

func AddBoolflag(cmd *cobra.Command, parameter *ParameterBool) {
	if cmd.Flags().Lookup(parameter.ID()) == nil {
		cmd.Flags().BoolVar(&parameter.Value, parameter.ID(), parameter.DefaultValue, parameter.Description)
	}
}

func AddUintSliceflag(cmd *cobra.Command, parameter *ParameterUintSlice) {
	if cmd.Flags().Lookup(parameter.ID()) == nil {
		cmd.Flags().UintSliceVar(&parameter.Value, parameter.ID(), parameter.DefaultValue, parameter.Description)
	}
}

func BindEnv(v *viper.Viper, parameter Parameter) {
	if contains(v.AllKeys(), parameter.ID()) {
		v.BindEnv(parameter.ID(), parameter.EnvironmentVariableName())
	}
}

func BindConfig(cmd *cobra.Command, v *viper.Viper, parameter Parameter) {
	if !contains(v.AllKeys(), parameter.ID()) {
		v.BindPFlag(parameter.ID(), cmd.Flags().Lookup(parameter.ID()))
	}
}

func contains(list []string, element string) bool {
	for _, e := range list {
		if e == element {
			return true
		}
	}
	return false
}
