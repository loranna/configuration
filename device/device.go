package device

import (
	"fmt"
	"math/rand"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/loranna/configuration/internal/configuration"
)

type Config struct {
	RabbitMQUrl          string  `yaml:"rabbitmqserverurl" json:"rabbitmqserverurl" mapstructure:"rabbitmqserverurl"`
	RabbitMQUsername     string  `yaml:"rabbitmqusername" json:"rabbitmqusername" mapstructure:"rabbitmqusername"`
	RabbitMQPassword     string  `yaml:"rabbitmqpassword" json:"rabbitmqpassword" mapstructure:"rabbitmqpassword"`
	RabbitMQPort         uint    `yaml:"rabbitmqport" json:"rabbitmqport" mapstructure:"rabbitmqport"`
	Deviceupqueuename    string  `yaml:"deviceupqueuename" json:"deviceupqueuename" mapstructure:"deviceupqueuename"`
	Gatewaydownqueuename string  `yaml:"gatewaydownqueuename" json:"gatewaydownqueuename" mapstructure:"gatewaydownqueuename"`
	ServerURL            string  `yaml:"environmentserverurl" json:"environmentserverurl" mapstructure:"environmentserverurl"`
	Serverport           uint    `yaml:"environmentserverport" json:"environmentserverport" mapstructure:"environmentserverport"`
	UplinkPayload        string  `yaml:"uplinkpayload" json:"uplinkpayload" mapstructure:"uplinkpayload"`
	Deveui               string  `yaml:"deveui" json:"deveui" mapstructure:"deveui"`
	Appeui               string  `yaml:"appeui" json:"appeui" mapstructure:"appeui"`
	Appkey               string  `yaml:"appkey" json:"appkey" mapstructure:"appkey"`
	Devaddr              string  `yaml:"devaddr" json:"devaddr" mapstructure:"devaddr"`
	Appskey              string  `yaml:"appskey" json:"appskey" mapstructure:"appskey"`
	Nwkskey              string  `yaml:"nwkskey" json:"nwkskey" mapstructure:"nwkskey"`
	Joinmethod           string  `yaml:"joinmethod" json:"joinmethod" mapstructure:"joinmethod"`
	Title                string  `yaml:"title" json:"title" mapstructure:"title"`
	Counterup            uint    `yaml:"counterup" json:"counterup" mapstructure:"counterup"`
	Counterdown          uint    `yaml:"counterdown" json:"counterdown" mapstructure:"counterdown"`
	Uplinkport           uint    `yaml:"uplinkport" json:"uplinkport" mapstructure:"uplinkport"`
	Uplinkinterval       uint    `yaml:"uplinkinterval" json:"uplinkinterval" mapstructure:"uplinkinterval"`
	Program              uint    `yaml:"program" json:"program" mapstructure:"program"`
	DeviceserverURL      string  `yaml:"deviceserverurl" json:"deviceserverurl" mapstructure:"deviceserverurl"`
	Deviceserverport     uint    `yaml:"deviceserverport" json:"deviceserverport" mapstructure:"deviceserverport"`
	Rejoininterval       uint    `yaml:"rejoininterval" json:"rejoininterval" mapstructure:"rejoininterval"`
	Rejoinretry          uint    `yaml:"rejoinretry" json:"rejoinretry" mapstructure:"rejoinretry"`
	Adr                  bool    `yaml:"adr" json:"adr" mapstructure:"adr"`
	UplinkConfirmed      bool    `yaml:"uplinkconfirmed" json:"uplinkconfirmed" mapstructure:"uplinkconfirmed"`
	Longitude            float64 `yaml:"longitude" json:"longitude" mapstructure:"longitude"`
	Latitude             float64 `yaml:"latitude" json:"latitude" mapstructure:"latitude"`
	Registermethod       string  `yaml:"registermethod" json:"registermethod" mapstructure:"registermethod"`
	LoriotServerURL      string  `yaml:"loriotserverurl" json:"loriotserverurl" mapstructure:"loriotserverurl"`
	LoriotServerAPIKey   string  `yaml:"loriotserverapikey" json:"loriotserverapikey" mapstructure:"loriotserverapikey"`
	Groups               []uint  `yaml:"groups" json:"groups" mapstructure:"groups"`
	Region               string  `yaml:"region" json:"region" mapstructure:"region"`
	VersionLoRaWAN       string  `yaml:"versionlorawan" json:"versionlorawan" mapstructure:"versionlorawan"`
	VersionRegion        string  `yaml:"versionregion" json:"versionregion" mapstructure:"versionregion"`
}

var varstrings, varints, varbools, varfloat64s, varuintslice = Load()

func Load() ([]configuration.ParameterString, []configuration.ParameterInt, []configuration.ParameterBool, []configuration.ParameterFloat64, []configuration.ParameterUintSlice) {
	var varstrings = []configuration.ParameterString{
		{Id: "rabbitmqserverurl", EnvVariableName: "RABBITMQ_SERVER_URL", Description: "URL of the RabbitMQ server used for data exchange", DefaultValue: "localhost"},
		{Id: "rabbitmqusername", EnvVariableName: "RABBITMQ_USERNAME", Description: "User Name of the RabbitMQ server used for data exchange", DefaultValue: "guest"},
		{Id: "rabbitmqpassword", EnvVariableName: "RABBITMQ_PASSWORD", Description: "Password of the RabbitMQ server used for data exchange", DefaultValue: "guest"},
		{Id: "deviceupqueuename", EnvVariableName: "DEVICE_UP_QUEUE_NAME", Description: "Name of the queue used for the devie uplink to the environment messages", DefaultValue: "uplinktoenvironment"},
		{Id: "gatewaydownqueuename", EnvVariableName: "GATEWAY_DOWN_QUEUE_NAME", Description: "Name of the queue used for the gateway downlink to the environment messages", DefaultValue: "downlinktoenvironment"},
		{Id: "environmentserverurl", EnvVariableName: "ENVIRONMENT_SERVER_URL", Description: "URL of the environment used to manipulate messages in both directions"},
		{Id: "uplinkpayload", EnvVariableName: "UPLINK_PAYLOAD", Description: "Value of the User payload (FRMPayload) in every uplink messages", DefaultValue: "0000"},
		{Id: "deveui", EnvVariableName: "DEVEUI", Description: "Deveui of the device according to LoRaWAN specification", DefaultValue: default8bytes()},
		{Id: "appeui", EnvVariableName: "APPEUI", Description: "Appeui of the device according to LoRaWAN specification", DefaultValue: default8bytes()},
		{Id: "appkey", EnvVariableName: "APPKEY", Description: "Appkey of the device according to LoRaWAN specification", DefaultValue: default16bytes()},
		{Id: "devaddr", EnvVariableName: "DEVADDR", Description: "Devaddr of the device according to LoRaWAN specification", DefaultValue: default4bytes()},
		{Id: "appskey", EnvVariableName: "APPSKEY", Description: "Appskey of the device according to LoRaWAN specification", DefaultValue: default16bytes()},
		{Id: "nwkskey", EnvVariableName: "NWKSKEY", Description: "Nwkskey of the device according to LoRaWAN specification", DefaultValue: default16bytes()},
		{Id: "joinmethod", EnvVariableName: "JOIN_METHOD", Description: "Join method of the device according to LoRaWAN specification. Possible values: OTAA,ABP", DefaultValue: "OTAA"},
		{Id: "title", EnvVariableName: "TITLE", Description: "title or name of the device", DefaultValue: "loranna"},
		{Id: "registermethod", EnvVariableName: "REGISTER_METHOD", Description: "Registration method of the device in LORIOT. Possible values: none,self,other,auto", DefaultValue: "auto"},
		{Id: "loriotserverurl", EnvVariableName: "LORIOT_SERVER_URL", Description: "URL of the LORIOT server used for data processing"},
		{Id: "loriotserverapikey", EnvVariableName: "LORIOT_SERVER_API_KEY", Description: "API key of the LORIOT server used for data processing"},
		{Id: "deviceserverurl", EnvVariableName: "DEVICE_SERVER_URL", Description: "URL of the device used to access it during execution"},
		{Id: "region", EnvVariableName: "REGION", Description: "Region of the device. Possible values: EU868,US915,CN779,EU433,AU915,CN470,AS923,KR920,IN865,RU864", DefaultValue: "EU868"},
		{Id: "versionlorawan", EnvVariableName: "VERSION_LORAWAN", Description: "LoRaWAN version of the device. Possible values: v1.0.0,v1.0.1", DefaultValue: "v1.0.0"},
		{Id: "versionregion", EnvVariableName: "VERSION_REGION", Description: "Region version of the device. Possible values: v1.0.0,v1.0.1", DefaultValue: "v1.0.0"},
	}

	var varints = []configuration.ParameterInt{
		{Id: "environmentserverport", EnvVariableName: "ENVIRONMENT_SERVER_PORT", Description: "Port of the environment used to manipulate messages in both directions", DefaultValue: 35999},
		{Id: "rabbitmqport", EnvVariableName: "RABBITMQ_PORT", Description: "Port of the RabbitMQ server used for data exchange", DefaultValue: 5672},
		{Id: "counterup", EnvVariableName: "COUNTER_UP", Description: "Value of the uplink counter (FCntUp) in the device side", DefaultValue: 0},
		{Id: "counterdown", EnvVariableName: "COUNTER_DOWN", Description: "Value of the downlink counter (FCntDown) in the device side", DefaultValue: 0},
		{Id: "uplinkport", EnvVariableName: "UPLINK_PORT", Description: "Value of port (FPort) used in every uplink message", DefaultValue: 22},
		{Id: "uplinkinterval", EnvVariableName: "UPLINK_INTERVAL", Description: "How often should the device send uplinks. Unit is second", DefaultValue: 5},
		{Id: "program", EnvVariableName: "PROGRAM", Description: "ID of the program used in the device", DefaultValue: 0},
		{Id: "deviceserverport", EnvVariableName: "DEVICE_SERVER_PORT", Description: "Port of the device used to access it during execution", DefaultValue: 36000},
		{Id: "rejoininterval", EnvVariableName: "REJOIN_INTERVAL", Description: "How often must the device perform a join again after an unsuccessful join", DefaultValue: 10},
		{Id: "rejoinretry", EnvVariableName: "REJOIN_RETRY", Description: "How many times must the device perform a join before giving up", DefaultValue: 10},
	}

	var varbools = []configuration.ParameterBool{
		{Id: "adr", EnvVariableName: "ADR", Description: "Enable/Disable the ADR (Adaptive Data Rate) in the device", DefaultValue: true},
		{Id: "uplinkconfirmed", EnvVariableName: "UPLINK_CONFIRMED", Description: "Enable/Disable confirmed uplink in the device", DefaultValue: false},
	}

	var varfloat64s = []configuration.ParameterFloat64{
		{Id: "longitude", EnvVariableName: "LONGITUDE", Description: "Longitude of the device", DefaultValue: 47.515086},
		{Id: "latitude", EnvVariableName: "LATITUDE", Description: "Latitude of the device", DefaultValue: 19.078076},
	}
	var varuintslice = []configuration.ParameterUintSlice{
		{Id: "groups", EnvVariableName: "GROUPS", Description: "comma separated list of group IDs which the device is the member of", DefaultValue: []uint{0}},
	}
	return varstrings, varints, varbools, varfloat64s, varuintslice
}

var random = rand.New(rand.NewSource(time.Now().UnixNano()))

func default8bytes() string {
	rnum := random.Uint64()
	return fmt.Sprintf("7AAAA5%010X", rnum&0x000000ffffffffff)
}
func default4bytes() string {
	rnum := random.Uint32()
	return fmt.Sprintf("%08X", rnum)
}
func default16bytes() string {
	rnumb := random.Uint64()
	rnuml := random.Uint64()
	return fmt.Sprintf("%016X%016X", rnumb, rnuml)
}

func AddAndBindFlags(cmd *cobra.Command, v *viper.Viper) {
	for i := 0; i < len(varstrings); i++ {
		configuration.AddStringflag(cmd, &varstrings[i])
		configuration.BindConfig(cmd, v, &varstrings[i])
	}
	for i := 0; i < len(varints); i++ {
		configuration.AddIntflag(cmd, &varints[i])
		configuration.BindConfig(cmd, v, &varints[i])
	}
	for i := 0; i < len(varbools); i++ {
		configuration.AddBoolflag(cmd, &varbools[i])
		configuration.BindConfig(cmd, v, &varbools[i])
	}
	for i := 0; i < len(varfloat64s); i++ {
		configuration.AddFloat64flag(cmd, &varfloat64s[i])
		configuration.BindConfig(cmd, v, &varfloat64s[i])
	}
	for i := 0; i < len(varuintslice); i++ {
		configuration.AddUintSliceflag(cmd, &varuintslice[i])
		configuration.BindConfig(cmd, v, &varuintslice[i])
	}
}

func Regenerate() {
	varstrings, varints, varbools, varfloat64s, varuintslice = Load()
}

func BindEnv(v *viper.Viper) {
	for i := 0; i < len(varstrings); i++ {
		configuration.BindEnv(v, &varstrings[i])
	}
	for i := 0; i < len(varints); i++ {
		configuration.BindEnv(v, &varints[i])
	}
	for i := 0; i < len(varbools); i++ {
		configuration.BindEnv(v, &varbools[i])
	}
	for i := 0; i < len(varfloat64s); i++ {
		configuration.BindEnv(v, &varfloat64s[i])
	}
	for i := 0; i < len(varuintslice); i++ {
		configuration.BindEnv(v, &varuintslice[i])
	}
}

func LoadConfig(v *viper.Viper) (*Config, error) {
	c := Config{}
	v.AddConfigPath(".")
	err := v.ReadInConfig()
	if err != nil {
		logrus.Infoln("config file was not found, using other resources of parameters")
	}
	//workaround for uint slice
	//https://github.com/spf13/viper/issues/926
	if value, ok := v.Get("groups").(string); ok {
		v.Set("groups", strings.Trim(value, "[]"))
	}
	//workaround ended
	err = v.UnmarshalExact(&c)
	if err != nil {
		return nil, err
	}
	return &c, nil
}

func Env(v *viper.Viper) []string {
	envs := []string{}
	for i := 0; i < len(varints); i++ {
		env := fmt.Sprintf("%s=%s", varints[i].EnvironmentVariableName(), v.GetString(varints[i].ID()))
		envs = append(envs, env)
	}
	for i := 0; i < len(varstrings); i++ {
		env := fmt.Sprintf("%s=%s", varstrings[i].EnvironmentVariableName(), v.GetString(varstrings[i].ID()))
		envs = append(envs, env)
	}
	for i := 0; i < len(varbools); i++ {
		env := fmt.Sprintf("%s=%s", varbools[i].EnvironmentVariableName(), v.GetString(varbools[i].ID()))
		envs = append(envs, env)
	}
	for i := 0; i < len(varfloat64s); i++ {
		env := fmt.Sprintf("%s=%s", varfloat64s[i].EnvironmentVariableName(), v.GetString(varfloat64s[i].ID()))
		envs = append(envs, env)
	}
	for i := 0; i < len(varuintslice); i++ {
		a := v.Get(varuintslice[i].ID()).([]interface{})
		env := fmt.Sprintf("%s=%s", varuintslice[i].EnvironmentVariableName(),
			strings.ReplaceAll(strings.Trim(fmt.Sprintf("%v", a), "[]"), " ", ","))
		envs = append(envs, env)
	}
	return envs
}
