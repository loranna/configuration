package environment

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/loranna/configuration/internal/configuration"
)

type Config struct {
	RabbitMQUrl          string `yaml:"rabbitmqserverurl" json:"rabbitmqserverurl" mapstructure:"rabbitmqserverurl"`
	RabbitMQUsername     string `yaml:"rabbitmqusername" json:"rabbitmqusername" mapstructure:"rabbitmqusername"`
	RabbitMQPassword     string `yaml:"rabbitmqpassword" json:"rabbitmqpassword" mapstructure:"rabbitmqpassword"`
	RabbitMQPort         uint   `yaml:"rabbitmqport" json:"rabbitmqport" mapstructure:"rabbitmqport"`
	Deviceupqueuename    string `yaml:"deviceupqueuename" json:"deviceupqueuename" mapstructure:"deviceupqueuename"`
	Gatewaydownqueuename string `yaml:"gatewaydownqueuename" json:"gatewaydownqueuename" mapstructure:"gatewaydownqueuename"`
	ServerURL            string `yaml:"environmentserverurl" json:"environmentserverurl" mapstructure:"environmentserverurl"`
	Serverport           uint   `yaml:"environmentserverport" json:"environmentserverport" mapstructure:"environmentserverport"`
}

var varstrings = [...]configuration.ParameterString{
	{Id: "rabbitmqserverurl", EnvVariableName: "RABBITMQ_SERVER_URL", Description: "URL of the RabbitMQ server used for data exchange", DefaultValue: "localhost"},
	{Id: "rabbitmqusername", EnvVariableName: "RABBITMQ_USERNAME", Description: "User Name of the RabbitMQ server used for data exchange", DefaultValue: "guest"},
	{Id: "rabbitmqpassword", EnvVariableName: "RABBITMQ_PASSWORD", Description: "Password of the RabbitMQ server used for data exchange", DefaultValue: "guest"},
	{Id: "deviceupqueuename", EnvVariableName: "DEVICE_UP_QUEUE_NAME", Description: "Name of the queue used for the devie uplink to the environment messages", DefaultValue: "uplinktoenvironment"},
	{Id: "gatewaydownqueuename", EnvVariableName: "GATEWAY_DOWN_QUEUE_NAME", Description: "Name of the queue used for the gateway downlink to the environment messages", DefaultValue: "downlinktoenvironment"},
	{Id: "environmentserverurl", EnvVariableName: "ENVIRONMENT_SERVER_URL", Description: "URL of the environment used to manipulate messages in both directions"},
}
var varints = [...]configuration.ParameterInt{
	{Id: "environmentserverport", EnvVariableName: "ENVIRONMENT_SERVER_PORT", Description: "Port of the environment used to manipulate messages in both directions", DefaultValue: 35999},
	{Id: "rabbitmqport", EnvVariableName: "RABBITMQ_PORT", Description: "Port of the RabbitMQ server used for data exchange", DefaultValue: 5672},
}

func AddAndBindFlags(cmd *cobra.Command, v *viper.Viper) {
	for i := 0; i < len(varstrings); i++ {
		configuration.AddStringflag(cmd, &varstrings[i])
		configuration.BindConfig(cmd, v, &varstrings[i])
	}
	for i := 0; i < len(varints); i++ {
		configuration.AddIntflag(cmd, &varints[i])
		configuration.BindConfig(cmd, v, &varints[i])
	}
}

func BindEnv(v *viper.Viper) {
	for i := 0; i < len(varstrings); i++ {
		configuration.BindEnv(v, &varstrings[i])
	}
	for i := 0; i < len(varints); i++ {
		configuration.BindEnv(v, &varints[i])
	}
}

func LoadConfig(v *viper.Viper) (*Config, error) {
	c := Config{}
	err := v.UnmarshalExact(&c)
	if err != nil {
		return nil, err
	}
	return &c, nil
}

func Env(v *viper.Viper) []string {
	envs := []string{}
	for i := 0; i < len(varints); i++ {
		env := fmt.Sprintf("%s=%s", varints[i].EnvironmentVariableName(), v.GetString(varints[i].ID()))
		envs = append(envs, env)
	}
	for i := 0; i < len(varstrings); i++ {
		env := fmt.Sprintf("%s=%s", varstrings[i].EnvironmentVariableName(), v.GetString(varstrings[i].ID()))
		envs = append(envs, env)
	}
	return envs
}
