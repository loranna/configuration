package docs

import (
	"os"
	"path"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/cobra/doc"
)

func Generate(rootCmd *cobra.Command) error {
	err := os.MkdirAll("docs", 0777)
	if err != nil {
		return err
	}
	err = doc.GenMarkdownTreeCustom(rootCmd, "./docs", filePrepender, linkHandler)
	if err != nil {
		return err
	}
	err = os.MkdirAll("completions", 0777)
	if err != nil {
		return err
	}
	err = rootCmd.GenBashCompletionFile("completions/bash.sh")
	if err != nil {
		return err
	}
	return nil
}

func filePrepender(filename string) string { return "" }

func linkHandler(name string) string {
	base := strings.TrimSuffix(name, path.Ext(name))
	return "#" + strings.ReplaceAll(strings.ToLower(base), "_", "-")
}
